# TypeScript Monorepo Experiment

This experiment uses a different approach than anything I have seen.

I am not sure if everything would work in actual real-life uses cases.

Looking for feedback.

## Approach

This approach takes advantage of NodeJS module discoverability - searching the tree for `node_modules` directory. That is why there is a `src/node_modules` directory.

## Advantages

### Single `tsconfig.json` file

Every monorepo setup I've seen uses a separate `tsconfig.json` file for each package.

This might indeed be desireable when a package needs a different configuration. E.g. mixing frontend and backend packages in a single repo.

One solution to that maybe having multiple config files in the root, and namespacing the packages further. E.g. `tsconfig.client.json` and have more nesting `src/node_modules/client/a`.

### Clean `package.json` in child modules

No need to put any additional run commands (e.g. `build`) into each individual child module.

## Disadvantages

No support for `lerna publish`, because everything is built into a single `./dst` dir.

## Testing

To test this approach, simply clone the repo, and then:

```sh
npm i
npm run build
npm start
```

